document.addEventListener("DOMContentLoaded", function() {
	const links = document.querySelectorAll('.navigate');

	for (const link of links) {
		link.addEventListener("click", clickHandler);
	}

	function clickHandler(e) {
		e.preventDefault();
		const target = document.querySelector(this.getAttribute("href"));
		const offsetTop = target.offsetTop;

		window.scrollTo({
			top: offsetTop,
			behavior: "smooth"
		});
	}
});